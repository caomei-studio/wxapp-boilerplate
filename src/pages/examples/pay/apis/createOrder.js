import request from 'wxapp-request';
import env from '../../../../env';

export default function createOrder(productType, itemId, num) {
  return request.post({
    url: `${env.API_URL}/pay/common/create_order?productType=${productType || ''}`,
    data: {
      itemId,
      num,
    },
  });
}
